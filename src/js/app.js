var app = new Vue({
    el: '#voiceRecognitionLayer',
    data: {
        voiceRecognitionStatus: false,
        showModal: true,
        message: null,
        botMessageQuantity: 0,
        conversationalInputId: 'TextField7',
        speechRecognitionSupportStatus: false,
        debug: false,
        labels: {
            turnedOn: 'Voice recognition is ON',
            turnedOff: 'Voice recognition is OFF',
            turnedOff: 'Voice recognition is OFF',
        }
    },
    methods: {
        testIfSpeechRecognitionSupported() {
            try {
                const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
                this.enableRecognition()
                return true
            }
            catch(e) {
                console.error(e);
                this.myLog('no-browser-support')
                return false
            }
        },
        enableRecognition() {
            const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
            this.recognition = new SpeechRecognition();
            this.recognition.continuous = true;
            this.recognition.lang = 'en-US';
            this.bindRecognitionListeners()
        },
        bindRecognitionListeners(){
            this.bindRecognitionOnResult()
            this.bindRecognitionOnStart()
            this.bindRecognitionOnSpeechEnd()
            this.bindRecognitionOnError()
        },
        bindRecognitionOnResult() {
            this.recognition.onresult = (event) => { // This block is called every time the Speech APi captures a line.
                const current = event.resultIndex;
                const transcript = event.results[current][0].transcript;
                const mobileRepeatBug = (current === 1 && transcript === event.results[0][0].transcript);

                this.disableVoiceRecognition()
                if(!mobileRepeatBug) {
                    this.message = transcript
                    this.pushMessage()
                }
            };
        },
        bindRecognitionOnStart() {
            this.recognition.onstart = () => {
                this.myLog('Voice recognition activated. Try speaking into the microphone.');
            }
        },
        bindRecognitionOnSpeechEnd() {
            this.recognition.onspeechend = () => {
                this.myLog('Voice recognition was turned off itself.');
                this.disableVoiceRecognition()
            }
        },
        bindRecognitionOnError() {
            this.recognition.onerror = (event) => {
                if(event.error === 'no-speech') {
                    this.myLog('No speech was detected. Try again.');
                };
                this.disableVoiceRecognition()
            }
        },
        toggleVoiceRecognition() {
            this.voiceRecognitionStatus = !this.voiceRecognitionStatus
        },
        disableVoiceRecognition() {
            this.voiceRecognitionStatus = false
        },
        pushMessage() {
            const assistantInputField = document.getElementById(this.conversationalInputId)
            assistantInputField.value = this.message
            this.sendEnterEvent(assistantInputField)
        },
        sendEnterEvent(el) {
            const config = {
                bubbles: true,
                charCode: 13,
                key: "Enter",
                // metaKey: false,
                // target: el,
                // type: "keypress",
                // which: 13,
                // altKey: false,
                // cancelable: true,
                // detail: 0,
                // eventPhase: 3,
                // isTrusted: true,
                // keyCode: 0,
                // locale: undefined,
                // location: 0,
                // nativeEvent: KeyboardEvent {isTrusted: true, key: "Enter", code: "Enter", location: 0, ctrlKey: false, …},
                // repeat: false,
                // shiftKey: false,
                // timeStamp: 1110431.8849999981,
                // view: window,
            }
            const e = new KeyboardEvent("keypress", config);
            el.dispatchEvent(e);
        },
        setBotMessageListener () {
            this.myLog('setBotMessageListener')
            setInterval(() => {
                const message = this.getBotLastMessages()
                if (message) this.readOutLoud(message)
            }, 1000)
        },
        getBotLastMessages () {
            const botMessages = [...document.querySelectorAll('.clever-bot-message')]
                .map(el => {
                    const doesContainImage = el.querySelectorAll('img').length
                    if(doesContainImage) return '{image}'
                    return el.textContent
                })
            const areCorrectMessages = !botMessages.some(m => m === '')
            const areNewMessages = botMessages.length > this.botMessageQuantity
            if (areNewMessages && areCorrectMessages) {
                const newBotMessage = botMessages
                    .slice(this.botMessageQuantity)
                    .join(' ')
                    .replace(/{image}/g, '');
                this.myLog('newBotMessage', newBotMessage)
                this.botMessageQuantity = botMessages.length
                return newBotMessage
            }
            return false
        },
        readOutLoud (message) {
            this.myLog('readOutLoud', message)
            const speech = new SpeechSynthesisUtterance();
            const  voices = window.speechSynthesis.getVoices();
            speech.voice = voices[3]; // Note: some voices don't support altering params
            speech.text = message;
            speech.lang = 'en-US';
            speech.volume = 1;
            speech.rate = 1;
            speech.pitch = 1;
            window.speechSynthesis.speak(speech);
        },
        closeModal () {
            this.showModal = false
        },
        setSpeech() {
            return new Promise(
                function (resolve, reject) {
                    let synth = window.speechSynthesis;
                    let id;
                    id = setInterval(() => {
                        if (synth.getVoices().length !== 0) {
                            resolve(synth.getVoices());
                            clearInterval(id);
                        }
                    }, 10);
                }
            )
        },
        myLog (label, value) {
            if(this.debug && label && value) {
                console.log(label, value)
                return
            }
            if(this.debug && label) console.log(label)
        }
    },
    computed: {
        buttonLabel () {
            return this.voiceRecognitionStatus ? this.labels.turnedOn : this.labels.turnedOff
        },
        modalInfo () {
            return this.speechRecognitionSupportStatus
                ? 'Voice Recognition is working for this Browser/Device'
                : 'Sorry, but Voice Recognition is not working for this Browser/Device'
        }
    },
    watch: {
        voiceRecognitionStatus () {
            if (this.voiceRecognitionStatus) this.recognition.start()
            else this.recognition.stop()
        },
        async showModal () {
            if (!this.showModal) {
                await this.setSpeech();
                this.setBotMessageListener()
            }
        }
    },
    mounted() {
        this.speechRecognitionSupportStatus = this.testIfSpeechRecognitionSupported()

    }
})
