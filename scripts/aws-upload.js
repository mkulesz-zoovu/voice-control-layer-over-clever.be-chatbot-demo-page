const fs = require('fs');
const path = require('path');
const glob = require(`glob`);
const chalk = require('chalk');
const mime = require('mime-types');
const AWS = require('aws-sdk');
const project = require('../package.json');


require('dotenv').config();

const BUCKET_KEY = process.env.BUCKET_KEY;
const MAX_AGE = 30;

const SRC_DIRECTORY = path.normalize(process.env.SRC_DIRECTORY);
const BUCKET_DIRECTORY = path.normalize(process.env.BUCKET_DIRECTORY);

AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
});

var s3 = new AWS.S3();

glob(path.join(SRC_DIRECTORY, '**/*'), function (error, filesToUpload) {
  let counter = 0;
  filesToUpload.filter(fileName => !!path.extname(fileName)).forEach((fileToUpload, index, filesToUpload) => {
    const normalizedFilePath = path.normalize(fileToUpload);
    const bucketPath = path.join(BUCKET_DIRECTORY, normalizedFilePath.replace(SRC_DIRECTORY, ''))

    const params = {
      Bucket: BUCKET_KEY,
      Body: fs.createReadStream(normalizedFilePath),
      Key: bucketPath.split(path.sep).join(path.posix.sep),
      /**
       * Content-Disposition can be used when would you like to upload auto downloadable file.
       * e.g. Content-Disposition: 'attachment;filename="file_name.ext"'
       * @link http://iwantmyreal.name/s3-download-only-presigned-upload
       */
      // ContentDisposition: 'inline',
      /**
       * Default content type is binary/octet-stream
       * @link https://stackoverflow.com/questions/46621282/how-to-generate-viewable-s3-url-nodejs
       */
      ContentType: mime.lookup(normalizedFilePath),
      CacheControl: `max-age=${MAX_AGE}`,
    };

    s3.upload(params, function (err, data) {
      counter = counter + 1;
      if (err) {
        console.log(`${chalk.red('Error:')} "${normalizedFilePath.split(path.sep).join(path.posix.sep)}"`);
        console.log('       ', chalk.red(err));
      }

      if (data) {
        const progress = Math.round((counter / filesToUpload.length) * 100);
        console.log(`${progress < 10 ? ' ' : ''}${progress < 100 ? ' ' : ''}[${progress}%] ${chalk.green('Uploaded:')}`, chalk.yellow('"' + normalizedFilePath.split(path.sep).join(path.posix.sep) + '"'), mime.lookup(normalizedFilePath));

        if (counter === filesToUpload.length) {
          console.log('');
          console.log(`    FINISHED`);
          console.log('');
        }
      }
    });
  });
});
